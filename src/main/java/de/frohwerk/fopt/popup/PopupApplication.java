package de.frohwerk.fopt.popup;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PopupApplication extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) {
        final Button showPopupButton = new Button("Show Popup");
        showPopupButton.setFont(new Font(32));
        showPopupButton.setOnAction(event -> showPopup());

        final BorderPane root = new BorderPane(showPopupButton);

        primaryStage.setTitle("Test-Application for JavaFX");
        primaryStage.setScene(new Scene(root, 640, 480));

        primaryStage.show(); // Starts JavaFX Application Thread (amongst others)
    }

    private void showPopup() {
        final Label greetingLabel = new Label("Hallo Welt");
        greetingLabel.setFont(new Font(32));

        final Stage popup = new Stage();
        popup.initModality(Modality.NONE);
        popup.setScene(new Scene(new BorderPane(greetingLabel), 320, 240));
        popup.showAndWait();

        System.out.println("After showAndWait()");
    }
}
