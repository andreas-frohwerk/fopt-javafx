package de.frohwerk.fopt;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class TemplateApplication extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) {
        final Label greetingLabel = new Label("Hallo Welt");
        greetingLabel.setFont(new Font(32));

        final BorderPane root = new BorderPane(greetingLabel);

        primaryStage.setTitle("Test-Application for JavaFX");
        primaryStage.setScene(new Scene(root, 640, 480));

        primaryStage.show(); // Starts JavaFX Application Thread (amongst others)
    }
}
