package de.frohwerk.fopt.paint;

import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Shape;

public interface DrawingStrategy {
    Shape begin(final MouseEvent event);

    void update(final MouseEvent event);

    void finish(final MouseEvent event);
}
