package de.frohwerk.fopt.paint;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.stream.Collectors;

public class ProtocolWindow extends Stage {
    private final TextArea textArea = new TextArea();

    private final ListChangeListener<Node> shapesChangeListener = change -> {
        while (change.next()) {
            textArea.appendText(change.getAddedSubList().stream().map(Node::toString).collect(Collectors.joining("\n")) + "\n");
        }
    };

    public ProtocolWindow(final ObservableList<Node> shapes) {
        shapes.addListener(shapesChangeListener);

        initModality(Modality.NONE);

        setScene(new Scene(textArea));
        setTitle("Protokoll");
        setAlwaysOnTop(true);
    }

}
