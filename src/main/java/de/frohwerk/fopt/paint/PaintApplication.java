package de.frohwerk.fopt.paint;

import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;

public class PaintApplication extends Application {
    private final IntegerProperty lineCounter = new SimpleIntegerProperty();
    private final IntegerProperty circleCounter = new SimpleIntegerProperty();
    private final IntegerProperty rectangleCounter = new SimpleIntegerProperty();
    private final IntegerProperty totalCounter = new SimpleIntegerProperty();

    private final BorderPane contentPane = new BorderPane();
    private final BorderPane rootPane = new BorderPane();

    private final Pane graphicsPane = new Pane();

    private final RadioButton lineButton = new RadioButton("Linie");
    private final RadioButton circleButton = new RadioButton("Kreis");
    private final RadioButton rectangleButton = new RadioButton("Rechteck");

    private final ToggleGroup shapeSelectionGroup = new ToggleGroup();
    private final HBox shapeSelectionBox = new HBox(10, lineButton, circleButton, rectangleButton);

    private final Button clearButton = new Button("Löschen");
    private final Button protocolButton = new Button("Protokoll");

    private final HBox actionButtons = new HBox(10, clearButton, protocolButton);

    private final Label lineCounterText = new Label();
    private final Label circleCounterText = new Label();
    private final Label rectangleCounterText = new Label();
    private final Label totalCounterText = new Label();

    private final HBox statusBar = new HBox(4,
            new Label("Linien: "), lineCounterText,
            new Label("Kreise: "), circleCounterText,
            new Label("Rechtecke: "), rectangleCounterText,
            new Label("Insgesamt: "), totalCounterText
    );

    private final DrawingStrategy line = new LineDrawing();
    private final DrawingStrategy circle = new CircleStrategy();
    private final DrawingStrategy rectangle = new RectangleStrategy();

    private final ProtocolWindow protocolWindow = new ProtocolWindow(graphicsPane.getChildren());

    private DrawingStrategy drawingStrategy = new LineDrawing();

    @Override
    public void start(final Stage primaryStage) {
        graphicsPane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        graphicsPane.setBorder(new Border(new BorderStroke(Color.DARKGREY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

        enableClipping(graphicsPane);

        graphicsPane.setOnMousePressed(event -> graphicsPane.getChildren().add(drawingStrategy.begin(event)));
        graphicsPane.setOnMouseDragged(event -> drawingStrategy.update(event));
        graphicsPane.setOnMouseReleased(event -> drawingStrategy.finish(event));

        clearButton.setOnAction(this::onClearAction);
        protocolButton.setOnAction(this::onProtocolAction);
        actionButtons.setPadding(new Insets(10, 0, 0, 0));

        lineButton.setSelected(true);

        lineButton.setOnAction(event -> drawingStrategy = line);
        circleButton.setOnAction(event -> drawingStrategy = circle);
        rectangleButton.setOnAction(event -> drawingStrategy = rectangle);

        shapeSelectionGroup.getToggles().addAll(lineButton, circleButton, rectangleButton);
        shapeSelectionBox.setPadding(new Insets(0, 0, 10, 0));

        statusBar.setPadding(new Insets(0, 6, 2, 6));
        totalCounter.bind(lineCounter.add(circleCounter).add(rectangleCounter));
        lineCounterText.textProperty().bind(lineCounter.asString());
        circleCounterText.textProperty().bind(circleCounter.asString());
        rectangleCounterText.textProperty().bind(rectangleCounter.asString());
        totalCounterText.textProperty().bind(totalCounter.asString());

        contentPane.setPadding(new Insets(10));
        contentPane.setTop(shapeSelectionBox);
        contentPane.setCenter(graphicsPane);
        contentPane.setBottom(actionButtons);

        rootPane.setCenter(contentPane);
        rootPane.setBottom(statusBar);

        primaryStage.setTitle("Test-Application for JavaFX");
        primaryStage.setScene(new Scene(rootPane, 640, 480));
        primaryStage.setOnCloseRequest(event -> {if (protocolWindow.isShowing()) protocolWindow.close();});

        primaryStage.show(); // Starts JavaFX Application Thread (amongst others)
    }

    private void enableClipping(final Pane pane) {
        final Rectangle clipRectangle = new Rectangle();

        clipRectangle.widthProperty().bind(this.graphicsPane.widthProperty());
        clipRectangle.heightProperty().bind(this.graphicsPane.heightProperty());

        pane.setClip(clipRectangle);
    }

    private void onClearAction(final ActionEvent event) {
        graphicsPane.getChildren().clear();
    }

    private void onProtocolAction(final ActionEvent event) {
        protocolWindow.show();
    }

    class LineDrawing implements DrawingStrategy {
        private Line line;

        @Override
        public Shape begin(final MouseEvent event) {
            line = new Line(event.getX(), event.getY(), event.getX(), event.getY());
            line.setStroke(Color.GREY);
            line.setStrokeWidth(2);
            return line;
        }

        @Override
        public void update(final MouseEvent event) {
            line.setEndX(event.getX());
            line.setEndY(event.getY());
        }

        @Override
        public void finish(final MouseEvent event) {
            line.setStroke(Color.BLACK);
            line.setStrokeWidth(5);
            lineCounter.set(lineCounter.get() + 1);
        }
    }

    class CircleStrategy implements DrawingStrategy {
        private Circle circle;

        @Override
        public Shape begin(final MouseEvent event) {
            circle = new Circle(1, Color.TRANSPARENT);
            circle.setCenterX(event.getX());
            circle.setCenterY(event.getY());
            circle.setStroke(Color.GREY);
            circle.setStrokeWidth(2);
            return circle;
        }

        @Override
        public void update(final MouseEvent event) {
            final double x = Math.abs(circle.getCenterX() - event.getX());
            final double y = Math.abs(circle.getCenterY() - event.getY());
            circle.setRadius(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
        }

        @Override
        public void finish(final MouseEvent event) {
            update(event);
            circle.setStroke(Color.BLACK);
            circle.setStrokeWidth(5);
            circleCounter.set(circleCounter.get() + 1);
        }
    }

    class RectangleStrategy implements DrawingStrategy {
        private Rectangle rectangle;

        private double startX;
        private double startY;

        @Override
        public Shape begin(final MouseEvent event) {
            startX = event.getX();
            startY = event.getY();

            rectangle = new Rectangle(0, 0, Color.TRANSPARENT);
            rectangle.setX(startX);
            rectangle.setY(startY);
            rectangle.setStroke(Color.GREY);
            rectangle.setStrokeWidth(2);

            return rectangle;
        }

        @Override
        public void update(final MouseEvent event) {
            final double width = event.getX() - startX;
            final double height = event.getY() - startY;

            rectangle.setWidth(width < 0 ? -width : width);
            rectangle.setHeight(height < 0 ? -height : height);

            if (width < 0) rectangle.setX(event.getX());
            if (height < 0) rectangle.setY(event.getY());
        }

        @Override
        public void finish(final MouseEvent event) {
            update(event);
            rectangle.setStroke(Color.BLACK);
            rectangle.setStrokeWidth(5);
            rectangleCounter.set(rectangleCounter.get() + 1);
        }
    }
}
