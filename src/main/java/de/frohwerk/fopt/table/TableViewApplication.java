package de.frohwerk.fopt.table;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class TableViewApplication extends Application {
    private final ObservableList<StringDoublePair> model = FXCollections.observableArrayList();

    private final TableView<StringDoublePair> tableView = new TableView<>(model);

    final TableView.TableViewSelectionModel<StringDoublePair> selectionModel = tableView.getSelectionModel();

    @Override
    public void start(final Stage stage) {
        model.add(new StringDoublePair("E8", 2583.21));
        model.add(new StringDoublePair("E9", 2749.89));

        final TableColumn<StringDoublePair, String> groupColumn = new TableColumn<>("Entgeltgruppe");
        groupColumn.setCellValueFactory(new PropertyValueFactory<>("group"));
        final TableColumn<StringDoublePair, String> salaryColumn = new TableColumn<>("Gehalt");
        salaryColumn.setCellValueFactory(new PropertyValueFactory<>("salary"));
        tableView.setOnMouseClicked(this::onTableClick);

        tableView.getColumns().addAll(groupColumn, salaryColumn);

        final Button addButton = new Button("Hinzufügen");
        addButton.setOnAction(this::onAddAction);
        final Button deleteButton = new Button("Löschen");
        deleteButton.setOnAction(this::onDeleteAction);
        final Button editButton = new Button("Ändern");
        editButton.setOnAction(this::onEditAction);
        final Button modifyAllButton = new Button("Alle Anpassen");
        modifyAllButton.setOnAction(this::onModifyAllButton);

        final HBox buttonRow = new HBox(10, addButton, editButton, deleteButton, modifyAllButton);
        buttonRow.setPadding(new Insets(10, 0, 0, 0));

        final BorderPane root = new BorderPane();
        root.setPadding(new Insets(10));

        root.setCenter(tableView);
        root.setBottom(buttonRow);

        stage.setTitle("Test-Application for JavaFX");
        stage.setScene(new Scene(root, 640, 480));

        stage.show();
    }

    private void onTableClick(final MouseEvent event) {
        if (event.getClickCount() > 1 && !tableView.getSelectionModel().isEmpty()) {
            new EditRowPopup(model, tableView.getSelectionModel().getFocusedIndex()).show();
        }
    }

    private void onAddAction(final ActionEvent event) {
        new EditRowPopup(model).show();
    }

    private void onDeleteAction(final ActionEvent event) {
        if (selectionModel.isEmpty()) {
            showErrorMessage("Es ist kein Eintrag in der Tabelle ausgewählt.");
        } else if (askForUserConfirmation("Wollen Sie den Eintrag wirklich löschen?")) {
            final int selection = selectionModel.getFocusedIndex();
            if (-1 < selection && selection < model.size()) {
                selectionModel.clearSelection();
                model.remove(selection);
            }
        }
    }

    private void onEditAction(final ActionEvent event) {
        if (selectionModel.isEmpty()) {
            showErrorMessage("Es ist kein Eintrag in der Tabelle ausgewählt.");
        } else {
            new EditRowPopup(model, tableView.getSelectionModel().getFocusedIndex()).show();
        }
    }

    private void onModifyAllButton(final ActionEvent event) {
        new AdaptPopup(model).show();
    }

    private boolean askForUserConfirmation(final String text) {
        final Alert alert = new Alert(Alert.AlertType.INFORMATION, null, ButtonType.OK, ButtonType.CANCEL);
        alert.setTitle("Bestätigung");
        alert.setHeaderText(text);
        alert.showAndWait();
        return alert.getResult() == ButtonType.OK;
    }

    private void showErrorMessage(final String text) {
        final Alert alert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
        alert.setTitle("Fehler");
        alert.setHeaderText(text);
        alert.show();
    }
}
