package de.frohwerk.fopt.table;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class EditRowPopup extends Stage {
    private final ObservableList<StringDoublePair> model;
    private final int row;

    private final TextField groupInput = new TextField();
    private final TextField salaryInput = new TextField();

    private final Label errorMessage = new Label();

    public EditRowPopup(final ObservableList<StringDoublePair> model) {
        this(model, -1);
    }

    public EditRowPopup(final ObservableList<StringDoublePair> model, final int row) {
        this.model = model;
        this.row = row;

        groupInput.setOnAction(this::onSubmitAction);
        salaryInput.setOnAction(this::onSubmitAction);

        errorMessage.setTextFill(Color.RED);

        if (-1 < row && row < model.size()) {
            final StringDoublePair pair = model.get(row);
            groupInput.setText(pair.getGroup());
            salaryInput.setText(Double.toString(pair.getSalary()));
        }

        final Button submitButton = new Button(row < 0 ? "Hinzufügen" : "Ändern");
        submitButton.setOnAction(this::onSubmitAction);

        final Button cancelButton = new Button("Abbrechen");
        cancelButton.setOnAction(event -> close());

        final VBox contentPane = new VBox(10,
                new HBox(10, new Label("Entgeltgruppe:"), groupInput),
                new HBox(10, new Label("Gehalt:"), salaryInput),
                new HBox(10, submitButton, cancelButton)
        );
        contentPane.setPadding(new Insets(10));

        final HBox statusBar = new HBox(errorMessage);
        statusBar.setPadding(new Insets(4));

        final BorderPane borderPane = new BorderPane();
        borderPane.setCenter(contentPane);
        borderPane.setBottom(statusBar);

        initModality(Modality.APPLICATION_MODAL);

        setTitle("Dialog zum " + (row < 0 ? "Hinzufügen" : "Ändern"));
        setScene(new Scene(borderPane));
        setMinWidth(320);
    }

    private void onSubmitAction(final ActionEvent actionEvent) {
        try {
            final String group = groupInput.getText();
            if (group == null || group.isEmpty()) {
                errorMessage.setText("Entgeltgruppe darf nicht leer sein");
                return;
            }
            final double salary = Double.parseDouble(salaryInput.getText());
            if (row < 0) {
                model.add(new StringDoublePair(group, salary));
            } else {
                final StringDoublePair row = model.get(this.row);
                row.setGroup(group);
                row.setSalary(salary);
            }
            close();
        } catch (final NumberFormatException ex) {
            errorMessage.setText("Invalid salary");
        }
    }
}
