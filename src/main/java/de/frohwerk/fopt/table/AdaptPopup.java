package de.frohwerk.fopt.table;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AdaptPopup extends Stage {
    private final ObservableList<StringDoublePair> model;

    private final TextField percentageInput = new TextField();

    private final Label errorMessage = new Label();

    public AdaptPopup(final ObservableList<StringDoublePair> model) {
        this.model = model;

        percentageInput.setOnAction(this::onSubmitAction);

        errorMessage.setTextFill(Color.RED);

        final Button submitButton = new Button("Anpassen");
        submitButton.setOnAction(this::onSubmitAction);

        final Button cancelButton = new Button("Abbrechen");
        cancelButton.setOnAction(event -> close());

        final VBox contentPane = new VBox(10,
                new HBox(10, new Label("Gehalt:"), percentageInput),
                new HBox(10, submitButton, cancelButton)
        );
        contentPane.setPadding(new Insets(10));

        final HBox statusBar = new HBox(errorMessage);
        statusBar.setPadding(new Insets(4));

        final BorderPane borderPane = new BorderPane();
        borderPane.setCenter(contentPane);
        borderPane.setBottom(statusBar);

        initModality(Modality.APPLICATION_MODAL);

        setTitle("Dialog zur Anpassung");
        setScene(new Scene(borderPane));
        setMinWidth(320);
    }

    private void onSubmitAction(final ActionEvent actionEvent) {
        try {
            final double percentage = Double.parseDouble(percentageInput.getText());
            for (final StringDoublePair pair : model) {
                pair.setSalary(pair.getSalary() * (1.0 + (percentage / 100)));
            }
            close();
        } catch (final NumberFormatException ex) {
            errorMessage.setText("Invalid percentage");
        }
    }
}
