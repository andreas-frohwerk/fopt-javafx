package de.frohwerk.fopt.table;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class StringDoublePair {
    private final SimpleStringProperty group;
    private final SimpleDoubleProperty salary;

    public StringDoublePair() {
        this("", 0);
    }

    public StringDoublePair(final String group, final double salary) {
        this.group = new SimpleStringProperty(group);
        this.salary = new SimpleDoubleProperty(salary);
    }

    public String getGroup() {
        return group.get();
    }

    public void setGroup(final String group) {
        this.group.set(group);
    }

    public SimpleStringProperty groupProperty() {
        return group;
    }

    public double getSalary() {
        return salary.get();
    }

    public void setSalary(final double salary) {
        this.salary.set(salary);
    }

    public SimpleDoubleProperty salaryProperty() {
        return salary;
    }
}
