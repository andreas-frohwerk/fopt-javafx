package de.frohwerk.fopt.mvp;

public class Item {
    private final String name;
    private final double weight;

    private boolean selected = false;

    public Item(final String name, final double weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
