package de.frohwerk.fopt.mvp;

import javafx.beans.binding.Bindings;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.util.Duration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BackpackPresenter {
    private static final Logger logger = LogManager.getLogger(BackpackPresenter.class);

    private final BackpackView view;

    private UpdatePercentageService updatePercentageService = new UpdatePercentageService();

    public BackpackPresenter(final BackpackView view, final BackpackModel model) {
        view.init(model.getAllItemNames(), model.getAllItemWeights(), model.getMaxWeight());

        view.percentageProperty().bind(
                Bindings.when(updatePercentageService.lastValueProperty().isNotNull())
                        .then(updatePercentageService.lastValueProperty())
                        .otherwise(view.percentageProperty().asObject())
        );

        view.setOnSelectionChange((name, selected) -> {
            model.setSelected(name, selected);
            view.updateTotalWeight(round(model.getSumOfUsedWeights()));
            view.updateEnabledItems(model.getEnabledItems());
            updatePercentageService.updateTarget(model.getSumOfUsedWeights() / model.getMaxWeight());
            updatePercentageService.restart();
        });

        this.view = view;
    }

    private static double round(final double value) {
        return Math.round(value * 100.0) / 100.0;
    }

    public Scene getView() {
        return view;
    }

    private class UpdatePercentageService extends ScheduledService<Double> {
        private double target = 0.0;
        private double difference = 0.0;
        private double step;

        UpdatePercentageService() {
            this.setPeriod(Duration.millis(2));
        }

        synchronized void updateTarget(final double target) {
            this.target = target;
            this.difference = target - view.getPercentage();
            this.step = difference > 0 ? 0.002 : -0.002;
        }

        @Override
        protected Task<Double> createTask() {
            return new Task<Double>() {
                @Override
                protected Double call() {
                    synchronized (UpdatePercentageService.this) {
                        difference -= step;
                        if (Math.abs(Math.round(difference * 100.0)) < 1) cancel();
                        final double current = view.getPercentage();
                        final double result = current + step;
//                        logger.info("current: {} | target: {} | step: {} | difference: {}", current, target, step, difference);
                        return result;
                    }
                }
            };
        }
    }

}
