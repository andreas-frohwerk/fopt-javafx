package de.frohwerk.fopt.mvp;

import javafx.application.Application;
import javafx.stage.Stage;

import java.util.Locale;

public class MvpApplication extends Application {
    private static final double MAX_WEIGHT = 10.0;

    private static final String[] ITEM_NAMES = {"Waschzeug", "Ersatzkleidung", "Bücher", "Medikamente", "Essenspaket 1", "Essenspaket 2", "Getränkemix 1", "Getränkemix 2"};
    private static final double[] ITEM_WEIGHTS = {1.2, 3.7, 3.3, 0.2, 1.5, 2.4, 4.7, 2.9};

    @Override
    public void start(final Stage primaryStage) {
        Locale.setDefault(Locale.US);

        final BackpackModel model = new BackpackModel(ITEM_NAMES, ITEM_WEIGHTS, MAX_WEIGHT);
        final BackpackView view = new BackpackView();

        final BackpackPresenter controller = new BackpackPresenter(view, model);
        primaryStage.setScene(controller.getView());

        primaryStage.setResizable(false);
        primaryStage.setTitle("Rucksack");
        primaryStage.show(); // Starts JavaFX Application Thread (amongst others)
    }
}
