package de.frohwerk.fopt.mvp;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.function.BiConsumer;

public class BackpackView extends Scene {
    private static final Logger logger = LogManager.getLogger(BackpackView.class);

    private final DoubleProperty maxWeight = new SimpleDoubleProperty(0.0);
    private final DoubleProperty totalWeightProperty = new SimpleDoubleProperty(0.0);
    private final DoubleProperty percentageProperty = new SimpleDoubleProperty(0.0);

    private final ObservableList<String> enabledItems = FXCollections.observableArrayList();

    private final ObservableList<Node> selectionControls;

    private BiConsumer<String, Boolean> selectionChangeListener = (name, selected) -> {};

    public BackpackView() {
        super(new Label("Loading..."));

        final BorderPane rootPane = new BorderPane();
        rootPane.setPadding(new Insets(20));

        // Configure weightTextContainer and add to the rootPane
        final Label totalWeightText = new Label();
        final Label maxWeightText = new Label();
        final VBox weightTextContainer = new VBox(10, totalWeightText, maxWeightText);

        totalWeightText.textProperty().bind(new SimpleStringProperty("Gesamtgewicht: ").concat(totalWeightProperty.asString()).concat(" kg"));
        maxWeightText.textProperty().bind(new SimpleStringProperty("Maximales Gewicht: ").concat(maxWeight.asString()).concat(" kg"));

        // Configure selectionsContainer and add to the rootPane
        final VBox selectionsContainer = new VBox(10);
        selectionsContainer.setPadding(new Insets(10, 0, 0, 0));

        final BorderPane controlsContainer = new BorderPane();
        controlsContainer.setTop(weightTextContainer);
        controlsContainer.setCenter(selectionsContainer);
        controlsContainer.setMinWidth(controlsContainer.getWidth() * 1.1);
        controlsContainer.setPadding(new Insets(0, 20, 0, 0));

        final Rectangle percentageBar = new Rectangle(0, 0, Color.BLUE);

        final Pane graphicsPane = enableClipping(new Pane(percentageBar));
        graphicsPane.setMinWidth(180);
        graphicsPane.setPadding(new Insets(10));
        graphicsPane.setBorder(new Border(new BorderStroke(Color.BLUE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

        // y = maxHeight * (1.0 - percentage) | z.B. 200 * (1.0 - 0.3)
        percentageBar.yProperty().bind(graphicsPane.heightProperty().multiply(new SimpleDoubleProperty(1.0).subtract(percentageProperty)));
        percentageBar.setX(0);
        // height = maxHeight - y
        percentageBar.heightProperty().bind(graphicsPane.heightProperty().subtract(percentageBar.yProperty()));
        percentageBar.widthProperty().bind(graphicsPane.widthProperty());

        percentageBar.xProperty().addListener((v, o, n) -> logger.info("Update x: {} =>Y {}", String.format("%.0f", o), String.format("%.0f", n)));
        percentageBar.heightProperty().addListener((v, o, n) -> logger.info("Update height: {} =>Y {}", String.format("%.0f", o), String.format("%.0f", n)));

        rootPane.setLeft(controlsContainer);
        rootPane.setCenter(graphicsPane);

        // Extract children list for dynamic addition of CheckBoxes
        selectionControls = selectionsContainer.getChildren();

        // Update the parent container
        setRoot(rootPane);
    }

    public void init(final List<String> names, final List<Double> weights, final double maxWeight) {
        if (names.size() != weights.size()) throw new IllegalArgumentException("names.size() != weights.size()");

        for (int i = 0; i < names.size(); i++) {
            final String itemName = names.get(i);
            final double itemWeight = weights.get(i);

            final CheckBox checkBox = new CheckBox(String.format("%s (%.2f kg)", itemName, itemWeight));

            final SimpleBooleanProperty selectableProperty = new SimpleBooleanProperty(true);
            checkBox.disableProperty().bind(checkBox.selectedProperty().or(selectableProperty).not());
            checkBox.selectedProperty().addListener((c, o, selected) -> selectionChangeListener.accept(itemName, selected));

            enabledItems.addListener((ListChangeListener<String>) c -> selectableProperty.set(c.getList().contains(itemName)));

            selectionControls.add(checkBox);
        }

        this.maxWeight.set(maxWeight);
    }

    public void setOnSelectionChange(final BiConsumer<String, Boolean> selectionChangeListener) {
        this.selectionChangeListener = selectionChangeListener;
    }

    public void updateTotalWeight(final double total) {
        this.totalWeightProperty.set(total);
    }

    public void updateEnabledItems(final List<String> enabledItems) {
        this.enabledItems.setAll(enabledItems);
    }

    public double getPercentage() {
        return percentageProperty.get();
    }

    public DoubleProperty percentageProperty() {
        return percentageProperty;
    }

    private Pane enableClipping(final Pane pane) {
        final Rectangle area = new Rectangle();
        area.heightProperty().bind(pane.heightProperty());
        area.widthProperty().bind(pane.widthProperty());
        pane.setClip(area);
        return pane;
    }
}




