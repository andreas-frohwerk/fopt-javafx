package de.frohwerk.fopt.mvp;

import java.util.*;
import java.util.stream.Collectors;

public class BackpackModel {
    private final double maxWeight;

    private final Map<String, Item> items = new LinkedHashMap<>();
    private final Collection<Item> values = Collections.unmodifiableCollection(items.values());

    public BackpackModel(final String[] names, final double[] weights, final double maxWeight) {
        if (names.length != weights.length) throw new IllegalArgumentException("names.length != weights.length");
        for (int i = 0; i < names.length; i++) {
            final String name = names[i];
            final double weight = weights[i];
            items.put(name, new Item(name, weight));
        }
        this.maxWeight = maxWeight;
    }

    public void setSelected(final String name, final boolean selected) {
        final Item item = items.get(name);
        if (item == null) throw new IllegalArgumentException("name '" + name + "'not found");
        item.setSelected(selected);
    }

    public double getSumOfUsedWeights() {
        double result = 0.0;
        for (final Item item : values) {
            if (item.isSelected()) result += item.getWeight();
        }
        return result;
    }

    public List<String> getAllItemNames() {
        final ArrayList<String> items = new ArrayList<>();
        for (Item item : values) {
            items.add(item.getName());
        }
        return items;
//        return values.stream().map(item -> item.getName()).collect(Collectors.toList());
//        return values.stream().map(Item::getName).collect(Collectors.toList());
    }

    public List<Double> getAllItemWeights() {
        return values.stream().map(Item::getWeight).collect(Collectors.toList());
    }

    public double getMaxWeight() {
        return maxWeight;
    }

    public List<String> getEnabledItems() {
        List<String> list = new ArrayList<>();
        for (Item item : values) {
            if (getSumOfUsedWeights() + item.getWeight() <= maxWeight) {
                String name = item.getName();
                list.add(name);
            }
        }
        return list;
    }
}
